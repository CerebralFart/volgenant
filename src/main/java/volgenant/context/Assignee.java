package volgenant.context;

import lombok.Getter;
import volgenant.context.label.Label;

import java.util.ArrayList;
import java.util.Collection;

public final class Assignee extends DataObject {
	@Getter
	private final Context context;
	@Getter
	private final User user;
	@Getter
	private Assignable assigned = null;
	@Getter
	private final Preferences preferences = new Preferences();

	Assignee(User user, Context context) {
		this.user = user;
		this.context = context;
	}

	@Override
	public String getName() {
		return this.user.getName();
	}

	public void assign(Assignable assignable) {
		this.resetAssignment();
		assignable.addAssignee(this);
		this.assigned = assignable;
	}

	public void resetAssignment() {
		if (this.assigned != null) {
			this.assigned.removeAssignee(this);
		}
		this.assigned = null;
	}

	@Override
	public Collection<String> getLabels() {
		Collection<String> userLabels = this.user.getLabels();
		Collection<String> labelAggregate = new ArrayList<>(this.labels.size() + userLabels.size());
		labelAggregate.addAll(this.labels);
		labelAggregate.addAll(userLabels);
		return labelAggregate;
	}

	@Override
	public String toString() {
		return String.format("Assignee(Context=%s, User=%s)", this.context.getId(), this.user.getName());
	}
}
