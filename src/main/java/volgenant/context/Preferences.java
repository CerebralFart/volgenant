package volgenant.context;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import volgenant.config.Config;

import java.util.LinkedList;

@Slf4j
public class Preferences {
	@Getter
	private int maxSize;
	private final LinkedList<Assignable> list = new LinkedList<>();

	public Preferences() {
		this(Config.instance.get("preferences.size", Integer.class));
	}

	public Preferences(int maxSize) {
		this.maxSize = maxSize;
	}

	public void setMaxSize(int size) {
		if (size != this.maxSize) {
			log.debug("Setting max size of PreferenceList@{} to {}", this.hashCode(), size);
			this.maxSize = size;
			this.cleanList();
		}
	}

	public void insertAt(Assignable assignable, int index) {
		if (index < this.maxSize) {
			if (this.list.contains(assignable)) {
				this.remove(assignable);
			}

			if (index > this.list.size()) {
				this.list.add(assignable);
			} else {
				this.list.add(index, assignable);
			}
			this.cleanList();
		}
	}

	public void remove(Assignable assignable) {
		this.list.remove(assignable);
	}

	public int size() {
		return this.list.size();
	}

	public Assignable get(int index) {
		return this.list.get(index);
	}

	public int indexOf(Assignable assignable) {
		return this.list.indexOf(assignable);
	}

	public boolean isEmpty(){
		return this.list.isEmpty();
	}

	private void cleanList() {
		int size = this.list.size();
		while (size > this.maxSize) {
			this.list.removeLast();
			size--;
		}
	}
}
