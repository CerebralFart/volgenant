package volgenant.context.label;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class LabeledObject {
	public static <T extends LabeledObject> List<T> findByLabel(Iterable<? extends T> objects, Label label) {
		List<T> result = new LinkedList<>();
		for (T object : objects) {
			if (label.matches(object)) {
				result.add(object);
			}
		}
		return result;
	}

	protected Collection<String> labels = new LinkedList<>();

	public void addLabel(String label) {
		this.labels.add(label);
	}

	public boolean hasLabel(String label) {
		return this.getLabels().contains(label);
	}

	public void removeLabel(String label) {
		this.labels.remove(label);
	}

	public Collection<String> getLabels() {
		return Collections.unmodifiableCollection(this.labels);
	}
}
