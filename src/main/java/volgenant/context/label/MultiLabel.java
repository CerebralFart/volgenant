package volgenant.context.label;

import lombok.EqualsAndHashCode;
import volgenant.Util;
import volgenant.context.DataObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

@EqualsAndHashCode(of = {"mode", "labels"})
public class MultiLabel implements Label {
	private static final Mode defaultMode = Mode.AND;

	private static Label[] mapLabels(String[] labels) {
		Label[] mapped = new Label[labels.length];
		for (int i = 0; i < labels.length; i++) {
			mapped[i] = new SingleLabel(labels[i]);
		}
		return mapped;
	}

	private Mode mode;
	private Collection<Label> labels;

	public MultiLabel(String... labels) {
		this(defaultMode, labels);
	}

	public MultiLabel(Label... labels) {
		this(defaultMode, labels);
	}

	public MultiLabel(Mode mode, String... labels) {
		this(mode, mapLabels(labels));
	}

	public MultiLabel(Mode mode, Label... labels) {
		this.mode = mode;
		this.labels = new ArrayList<>(labels.length);
		this.labels.addAll(Arrays.asList(labels));
	}

	@Override
	public boolean matches(LabeledObject object) {
		boolean match = this.mode.identity;
		for (Label label : this.labels) {
			boolean localMatch = label.matches(object);
			match = this.mode.reducer.apply(match, localMatch);
		}
		return match;
	}

	@Override
	public String toString() {
		return String.format("%s(%s)", this.mode, Util.join(this.labels.toArray(), ", ", Object::toString));
	}

}
