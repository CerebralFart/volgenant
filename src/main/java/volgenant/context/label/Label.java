package volgenant.context.label;

public interface Label {
	boolean matches(LabeledObject object);
}
