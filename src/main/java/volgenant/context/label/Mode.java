package volgenant.context.label;

import java.util.function.BinaryOperator;

public enum Mode {
	OR(false, (b1, b2) -> b1 || b2),
	AND(true, (b1, b2) -> b1 && b2);

	public final boolean identity;
	public final BinaryOperator<Boolean> reducer;

	Mode(boolean identity, BinaryOperator<Boolean> reducer) {
		this.identity = identity;
		this.reducer = reducer;
	}
}
