package volgenant.context.label;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode(of = "label")
public class SingleLabel implements Label {
	private final String label;

	public SingleLabel(String label) {
		this.label = label;
	}

	@Override
	public boolean matches(LabeledObject object) {
		return object.hasLabel(this.label);
	}

	@Override
	public String toString() {
		return this.label;
	}
}
