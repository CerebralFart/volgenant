package volgenant.context;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Slf4j
public class Assignable extends DataObject {
	@Getter
	private final String name;
	@Getter
	@Setter
	private int places = 1;
	private Set<Assignee> assignees = new HashSet<>(this.places);

	Assignable(String name) {
		this.name = name;
	}

	synchronized void addAssignee(Assignee assignee) throws IllegalStateException {
		if (this.getRemainingPlaces() < 1) throw new IllegalStateException("Assignable has no places left");

		log.debug("Adding assignee {} to assignable {}", assignee.getName(), this.name);
		this.assignees.add(assignee);
	}

	synchronized void removeAssignee(Assignee assignee) {
		log.debug("Removing assignee {} from assignable {}", assignee.getName(), this.name);
		this.assignees.remove(assignee);
	}

	public Collection<Assignee> getAssignees() {
		log.trace("Getting assignees for assignable {}", this.name);
		return Collections.unmodifiableCollection(this.assignees);
	}

	public void resetAssignees() {
		log.debug("Resetting assignees for assignable {}", this.name);
		this.assignees = new HashSet<>(this.places);
	}

	public int getAssignedCount() {
		log.trace("Getting assigned count for assignable {}", this.name);
		return this.assignees.size();
	}

	public int getRemainingPlaces() {
		log.trace("Getting remaining places count for assignable {}", this.name);
		int remaining = this.places - this.assignees.size();
		return Math.max(remaining, 0);
	}
}
