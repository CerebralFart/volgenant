package volgenant.context;

import volgenant.context.label.LabeledObject;

import java.util.Optional;

public abstract class DataObject extends LabeledObject {
	public static <T extends DataObject> Optional<T> findByName(Iterable<? extends T> objects, String name) {
		for (T object : objects) {
			if (object.getName().equals(name)) {
				return Optional.of(object);
			}
		}
		return Optional.empty();
	}

	public abstract String getName();
}
