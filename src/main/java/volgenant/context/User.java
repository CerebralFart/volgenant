package volgenant.context;

import lombok.Getter;
import volgenant.context.label.LabeledObject;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class User extends LabeledObject {
	@Getter
	private final String name;
	private final Set<Context> adminContexts = new HashSet<>();
	private final Map<Context, Assignee> availableContexts = new HashMap<>(1);

	public User(String name) {
		this.name = name;
	}

	void addAdminContext(Context context) {
		this.adminContexts.add(context);
	}

	void removeAdminContext(Context context) {
		this.adminContexts.remove(context);
	}

	public boolean isAdminFor(Context context) {
		return this.adminContexts.contains(context);
	}

	void addAvailableContext(Context context, Assignee assignee) {
		this.availableContexts.put(context, assignee);
	}

	void removeAvailableContext(Context context) {
		this.availableContexts.remove(context);
	}

	public Collection<Context> listAvailableContexts() {
		return Collections.unmodifiableCollection(this.availableContexts.keySet());
	}

	public Optional<Assignee> getForContext(Context context) {
		return Optional.ofNullable(this.availableContexts.get(context));
	}

	@Override
	public String toString() {
		return String.format("User(%s)", this.name);
	}
}
