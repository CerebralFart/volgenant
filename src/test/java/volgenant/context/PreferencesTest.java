package volgenant.context;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PreferencesTest {
	private Preferences list;

	@BeforeEach
	void setup() {
		this.list = new Preferences(5);
	}

	@Test
	void testGetMaxSize() {
		Assertions.assertEquals(5, this.list.getMaxSize(), "Expected default max size");
	}

	@Test
	void testSetMaxSize() {
		this.list.setMaxSize(7);
		Assertions.assertEquals(7, this.list.getMaxSize(), "Expected max size to be updated");
	}

	@Test
	void testSetMaxSizeFilledList() {
		Assignable assignable1 = new Assignable("Unimportant");
		this.list.insertAt(assignable1, 0);
		Assignable assignable2 = new Assignable("Unimportant");
		this.list.insertAt(assignable2, 1);
		Assignable assignable3 = new Assignable("Unimportant");
		this.list.insertAt(assignable3, 2);
		Assignable assignable4 = new Assignable("Unimportant");
		this.list.insertAt(assignable4, 3);
		Assignable assignable5 = new Assignable("Unimportant");
		this.list.insertAt(assignable5, 4);
		this.list.setMaxSize(3);
		Assertions.assertEquals(assignable1, this.list.get(0));
		Assertions.assertEquals(assignable2, this.list.get(1));
		Assertions.assertEquals(assignable3, this.list.get(2));
		Assertions.assertEquals(3, this.list.size());
	}

	@Test
	void testAddAssignable() {
		Assertions.assertEquals(0, this.list.size(), "Expected empty list");
		Assignable assignable1 = new Assignable("Unimportant");
		this.list.insertAt(assignable1, 0);
		Assertions.assertEquals(1, this.list.size(), "Expected list to contain 1 item");
	}

	@Test
	void testAddAssignableWReordering() {
		Assignable assignable1 = new Assignable("Unimportant");
		Assignable assignable2 = new Assignable("Unimportant");
		this.list.insertAt(assignable1, 0);
		this.list.insertAt(assignable2, 0);
		Assertions.assertEquals(assignable1, this.list.get(1));
		Assertions.assertEquals(assignable2, this.list.get(0));
	}

	@Test
	void testGet() {
		Assignable assignable1 = new Assignable("Unimportant");
		Assignable assignable2 = new Assignable("Unimportant");
		this.list.insertAt(assignable1, 0);
		this.list.insertAt(assignable2, 1);
		Assertions.assertEquals(assignable1, this.list.get(0));
		Assertions.assertEquals(assignable2, this.list.get(1));
	}

	@Test
	void testGetBumpedAssignable() {
		Assignable assignable1 = new Assignable("Unimportant");
		this.list.insertAt(assignable1, 0);
		Assignable assignable2 = new Assignable("Unimportant");
		this.list.insertAt(assignable2, 0);
		Assignable assignable3 = new Assignable("Unimportant");
		this.list.insertAt(assignable3, 0);
		Assignable assignable4 = new Assignable("Unimportant");
		this.list.insertAt(assignable4, 0);
		Assignable assignable5 = new Assignable("Unimportant");
		this.list.insertAt(assignable5, 0);
		Assignable assignable6 = new Assignable("Unimportant");
		this.list.insertAt(assignable6, 0);
		Assertions.assertEquals(5, this.list.size());
		Assertions.assertEquals(-1, this.list.indexOf(assignable1));
		Assertions.assertEquals(4, this.list.indexOf(assignable2));
		Assertions.assertEquals(3, this.list.indexOf(assignable3));
		Assertions.assertEquals(2, this.list.indexOf(assignable4));
		Assertions.assertEquals(1, this.list.indexOf(assignable5));
		Assertions.assertEquals(0, this.list.indexOf(assignable6));
	}

	@Test
	void testSize() {
		Assertions.assertEquals(0, this.list.size(), "Expected empty list");
		Assignable assignable1 = new Assignable("Unimportant");
		this.list.insertAt(assignable1, 0);
		Assertions.assertEquals(1, this.list.size(), "Expected list to contain 1 item");
		Assignable assignable2 = new Assignable("Unimportant");
		this.list.remove(assignable2);
		Assertions.assertEquals(1, this.list.size(), "Expected list to still contain 1 item");
		this.list.remove(assignable1);
		Assertions.assertEquals(0, this.list.size(), "Expected list to be emptied");
	}

	@Test
	void testSizeAfterResize() {
		this.list.insertAt(new Assignable("Unimportant"), 0);
		this.list.insertAt(new Assignable("Unimportant"), 0);
		this.list.insertAt(new Assignable("Unimportant"), 0);
		this.list.insertAt(new Assignable("Unimportant"), 0);
		this.list.insertAt(new Assignable("Unimportant"), 0);
		Assertions.assertEquals(5, this.list.size());
		this.list.setMaxSize(3);
		Assertions.assertEquals(3, this.list.size());
	}

	@Test
	void testRemove() {
		Assignable assignable1 = new Assignable("Unimportant");
		Assignable assignable2 = new Assignable("Unimportant");
		Assignable assignable3 = new Assignable("Unimportant");
		this.list.insertAt(assignable1, 0);
		this.list.insertAt(assignable2, 0);
		this.list.insertAt(assignable3, 0);
		Assertions.assertEquals(3, this.list.size());
		this.list.remove(assignable2);
		Assertions.assertEquals(assignable1, this.list.get(1));
		Assertions.assertEquals(assignable3, this.list.get(0));
	}

	@Test
	void testInsert() {
		Assertions.assertEquals(0, this.list.size(), "Expected empty list");
		Assignable assignable1 = new Assignable("Unimportant");
		this.list.insertAt(assignable1, 0);
		Assertions.assertEquals(1, this.list.size(), "Expected list to contain 1 item");
		Assertions.assertEquals(0, this.list.indexOf(assignable1), "Expected assignable to be at the top of the list");
	}

	@Test
	void testInsertNotConnected() {
		Assertions.assertEquals(0, this.list.size(), "Expected empty list");
		Assignable assignable1 = new Assignable("Unimportant");
		this.list.insertAt(assignable1, 3);
		Assertions.assertEquals(1, this.list.size(), "Expected list to contain 1 item");
		Assertions.assertEquals(0, this.list.indexOf(assignable1), "Expected assignable to be at the top of the list");
	}

	@Test
	void testInsertPastMaxSize() {
		Assertions.assertEquals(0, this.list.size(), "Expected empty list");
		Assignable assignable1 = new Assignable("Unimportant");
		this.list.insertAt(assignable1, 6);
		Assertions.assertEquals(0, this.list.size(), "Expected list remain empty when adding outside bounds");
	}

	@Test
	void testDuplicateInsertion() {
		Assignable assignable1 = new Assignable("Unimportant");
		Assignable assignable2 = new Assignable("Unimportant");
		this.list.insertAt(assignable1, 0);
		this.list.insertAt(assignable2, 0);
		this.list.insertAt(assignable1, 0);
		Assertions.assertEquals(2, this.list.size());
		Assertions.assertEquals(0, this.list.indexOf(assignable1));
		Assertions.assertEquals(1, this.list.indexOf(assignable2));
	}

	@Test
	void testIsEmpty(){
		Assignable assignable=new Assignable("Unimportant");
		Assertions.assertTrue(this.list.isEmpty());
		this.list.insertAt(assignable,0);
		Assertions.assertFalse(this.list.isEmpty());
		this.list.remove(assignable);
		Assertions.assertTrue(this.list.isEmpty());
	}
}
