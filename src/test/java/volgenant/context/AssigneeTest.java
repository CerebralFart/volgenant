package volgenant.context;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.UUID;

class AssigneeTest {

	@Test
	void testNaming() {
		String name = UUID.randomUUID().toString();
		Assignee assignee = new Assignee(new User(name), new Context());
		Assertions.assertEquals(name, assignee.getName());
		Assertions.assertTrue(assignee.toString().contains(name));
	}

	@Test
	void testLabelCascading() {
		User user = new User("Unimportant");
		user.addLabel("user-label");
		Assignee assignee = new Assignee(user, new Context());
		assignee.addLabel("assignee-label");

		Assertions.assertTrue(assignee.hasLabel("user-label"));
		Assertions.assertTrue(assignee.hasLabel("assignee-label"));
	}

	@Test
	void testAssign() {
		Assignee assignee = new Assignee(new User("Unimportant"), new Context());
		Assignable assignable = new Assignable("Unimportant");

		assignee.assign(assignable);
		Assertions.assertEquals(assignable, assignee.getAssigned());
		Assertions.assertTrue(assignable.getAssignees().contains(assignee));
	}

	@Test
	void testReassign() {
		Assignee assignee = new Assignee(new User("Unimportant"), new Context());
		Assignable assignable1 = new Assignable("Unimportant");
		Assignable assignable2 = new Assignable("Unimportant");

		assignee.assign(assignable1);
		Assertions.assertEquals(assignable1, assignee.getAssigned());
		Assertions.assertTrue(assignable1.getAssignees().contains(assignee));

		assignee.assign(assignable2);
		Assertions.assertEquals(assignable2, assignee.getAssigned());
		Assertions.assertTrue(assignable2.getAssignees().contains(assignee));
		Assertions.assertFalse(assignable1.getAssignees().contains(assignee));
	}

	@Test
	void testResetAssignment() {
		Assignee assignee = new Assignee(new User("Unimportant"), new Context());
		Assignable assignable = new Assignable("Unimportant");

		Assertions.assertNull(assignee.getAssigned());

		assignee.assign(assignable);
		Assertions.assertEquals(assignable, assignee.getAssigned());

		assignee.resetAssignment();
		Assertions.assertNull(assignee.getAssigned());
	}
}
