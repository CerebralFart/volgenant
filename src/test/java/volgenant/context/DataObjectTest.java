package volgenant.context;

import lombok.Getter;
import lombok.Setter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

class DataObjectTest {
	@Test
	void testFindByName() {
		DataObjectMock matching = new DataObjectMock();
		matching.setName("Matching");
		DataObjectMock notMatching = new DataObjectMock();
		notMatching.setName("NotMatching");

		Optional<DataObject> matchingOptional = DataObject.findByName(Arrays.asList(matching, notMatching), "Matching");
		Assertions.assertTrue(matchingOptional.isPresent());
		Assertions.assertEquals(matching, matchingOptional.get());

		Optional<DataObject> notFound = DataObject.findByName(Arrays.asList(matching, notMatching), "NotPresent");
		Assertions.assertFalse(notFound.isPresent());
	}

	@Test
	void testFindByNameEmpty() {
		Optional<DataObject> emptyResult = DataObject.findByName(new ArrayList<>(), "Unimportant");
		Assertions.assertFalse(emptyResult.isPresent());
	}

	private static class DataObjectMock extends DataObject {
		@Getter
		@Setter
		private String name;
	}
}
