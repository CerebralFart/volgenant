package volgenant.context;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.UUID;

class AssignableTest {
	private Assignable assignable;
	private Assignee assignee;

	@BeforeEach
	void init() {
		this.assignable = new Assignable(UUID.randomUUID().toString());
		this.assignee = new Assignee(
			new User("Unimportant"),
			new Context()
		);
	}

	@Test
	void testGetName() {
		String name = UUID.randomUUID().toString();
		Assignable assignable = new Assignable(name);
		Assertions.assertEquals(name, assignable.getName());
	}

	@Test
	void testGetSetPlaces() {
		this.assignable.setPlaces(3);
		Assertions.assertEquals(3, this.assignable.getPlaces());
		this.assignable.setPlaces(5);
		Assertions.assertEquals(5, this.assignable.getPlaces());
	}

	@Test
	void testAddAssignee() {
		this.assignable.addAssignee(this.assignee);
		Assertions.assertTrue(this.assignable.getAssignees().contains(this.assignee));
	}

	@Test
	void testAddAssigneeThrows() {
		this.assignable.setPlaces(0);
		Assertions.assertThrows(IllegalStateException.class, () -> {
			this.assignable.addAssignee(this.assignee);
		});
	}

	@Test
	void testAddAssigneeDuplicate() {
		this.assignable.setPlaces(2);
		Assertions.assertEquals(0, this.assignable.getAssignedCount());
		this.assignable.addAssignee(this.assignee);
		Assertions.assertEquals(1, this.assignable.getAssignedCount());
		this.assignable.addAssignee(this.assignee);
		Assertions.assertEquals(1, this.assignable.getAssignedCount());
	}

	@Test
	void testDerivedPlaces() {
		Assignee assignee1 = new Assignee(
			new User("Unimportant"),
			new Context()
		);
		Assignee assignee2 = new Assignee(
			new User("Unimportant"),
			new Context()
		);
		Assignee assignee3 = new Assignee(
			new User("Unimportant"),
			new Context()
		);

		this.assignable.setPlaces(1);
		this.assignable.addAssignee(assignee1);
		Assertions.assertEquals(1, this.assignable.getAssignedCount());
		Assertions.assertEquals(0, this.assignable.getRemainingPlaces());

		this.assignable.setPlaces(3);
		Assertions.assertEquals(1, this.assignable.getAssignedCount());
		Assertions.assertEquals(2, this.assignable.getRemainingPlaces());

		this.assignable.addAssignee(assignee2);
		this.assignable.addAssignee(assignee3);
		Assertions.assertEquals(3, this.assignable.getAssignedCount());
		Assertions.assertEquals(0, this.assignable.getRemainingPlaces());
	}

	@Test
	void testRemoveAssignee() {
		this.assignable.addAssignee(this.assignee);
		Assertions.assertTrue(this.assignable.getAssignees().contains(this.assignee));
		this.assignable.removeAssignee(this.assignee);
		Assertions.assertFalse(this.assignable.getAssignees().contains(this.assignee));
	}

	@Test
	void testResetAssignees() {
		this.assignable.addAssignee(this.assignee);
		Assertions.assertEquals(1, this.assignable.getAssignedCount());
		this.assignable.resetAssignees();
		Assertions.assertEquals(0, this.assignable.getAssignedCount());
	}
}
