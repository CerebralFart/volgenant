package volgenant.context;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;
import java.util.UUID;

class UserTest {
	private User user;

	@BeforeEach
	void init() {
		this.user = new User(UUID.randomUUID().toString());
	}

	@Test
	void testNaming() {
		String name = UUID.randomUUID().toString();
		User user = new User(name);
		Assertions.assertEquals(name, user.getName());
		Assertions.assertTrue(user.toString().contains(name));
	}

	@Test
	void testAdminContexts() {
		Context context = new Context();
		Assertions.assertFalse(this.user.isAdminFor(context));
		this.user.addAdminContext(context);
		Assertions.assertTrue(this.user.isAdminFor(context));
		this.user.removeAdminContext(context);
		Assertions.assertFalse(this.user.isAdminFor(context));
	}

	@Test
	void testAvailableContexts() {
		Context context1 = new Context();
		Context context2 = new Context();
		this.user.addAvailableContext(context1, new Assignee(this.user, context1));

		Assertions.assertTrue(this.user.listAvailableContexts().contains(context1));
		Assertions.assertFalse(this.user.listAvailableContexts().contains(context2));

		Optional<Assignee> c1q = this.user.getForContext(context1);
		Assertions.assertTrue(c1q.isPresent());
		Assertions.assertEquals(this.user, c1q.get().getUser());

		Assertions.assertFalse(this.user.getForContext(context2).isPresent());

		this.user.removeAvailableContext(context1);
		Assertions.assertFalse(this.user.getForContext(context1).isPresent());
	}
}
