package volgenant.context.label;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SingleLabelTest {
	private static final String text = "CompletelyUnimportant";
	private static final SingleLabel label = new SingleLabel(text);

	private LabeledObject object;

	@BeforeEach
	void init() {
		this.object = new LabeledObject();
	}

	@Test
	void testMatch() {
		this.object.addLabel(text);
		Assertions.assertTrue(label.matches(object));
	}

	@Test
	void testNoMatch() {
		this.object.addLabel(text + "Extension");
		Assertions.assertFalse(label.matches(object));
	}

	@Test
	void testEqualsAndHashCode() {
		SingleLabel copy = new SingleLabel(text);
		Assertions.assertTrue(label.equals(copy));
		Assertions.assertTrue(copy.equals(label));
		Assertions.assertEquals(label.hashCode(), copy.hashCode());
	}

	@Test
	void testToString() {
		Assertions.assertTrue(label.toString().contains(text));
	}
}
