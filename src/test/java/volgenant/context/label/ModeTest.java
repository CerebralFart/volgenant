package volgenant.context.label;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ModeTest {
	@Test
	void testIdentityReducerPair() {
		for (Mode mode : Mode.values()) {
			Assertions.assertEquals(mode.reducer.apply(true, mode.identity), true);
			Assertions.assertEquals(mode.reducer.apply(false, mode.identity), false);
		}
	}
}
