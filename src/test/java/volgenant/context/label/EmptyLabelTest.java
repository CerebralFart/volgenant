package volgenant.context.label;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.UUID;

class EmptyLabelTest {
	private EmptyLabel label = new EmptyLabel();

	@Test
	void testMatchesEmpty() {
		LabeledObject object = new LabeledObject();
		Assertions.assertTrue(this.label.matches(object));
	}

	@Test
	void testMatchesNonEmpty() {
		LabeledObject object = new LabeledObject();
		object.addLabel(UUID.randomUUID().toString());
		Assertions.assertTrue(this.label.matches(object));
	}
}
