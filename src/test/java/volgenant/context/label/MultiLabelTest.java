package volgenant.context.label;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.UUID;

class MultiLabelTest {
	private String label1;
	private String label2;
	private LabeledObject matchBoth;
	private LabeledObject match1;
	private LabeledObject match2;
	private LabeledObject matchNone;

	@BeforeEach
	void init() {
		this.label1 = UUID.randomUUID().toString();
		this.label2 = UUID.randomUUID().toString();
		this.matchBoth = new LabeledObject();
		this.matchBoth.addLabel(this.label1);
		this.matchBoth.addLabel(this.label2);
		this.match1 = new LabeledObject();
		this.match1.addLabel(this.label1);
		this.match2 = new LabeledObject();
		this.match2.addLabel(this.label2);
		this.matchNone = new LabeledObject();
	}

	@Test
	void testAnd() {
		Label and = new MultiLabel(Mode.AND, this.label1, this.label2);
		Assertions.assertTrue(and.matches(this.matchBoth));
		Assertions.assertFalse(and.matches(this.match1));
		Assertions.assertFalse(and.matches(this.match2));
		Assertions.assertFalse(and.matches(this.matchNone));
	}

	@Test
	void testOr() {
		Label or = new MultiLabel(Mode.OR, this.label1, this.label2);
		Assertions.assertTrue(or.matches(this.matchBoth));
		Assertions.assertTrue(or.matches(this.match1));
		Assertions.assertTrue(or.matches(this.match2));
		Assertions.assertFalse(or.matches(this.matchNone));
	}

	@Test
	void testToString() {
		Label label = new MultiLabel(this.label1, this.label2);
		String string = label.toString();
		Assertions.assertTrue(string.contains(this.label1));
		Assertions.assertTrue(string.contains(this.label2));
	}
}
