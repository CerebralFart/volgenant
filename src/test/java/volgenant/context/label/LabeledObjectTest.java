package volgenant.context.label;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

class LabeledObjectTest {
	private LabeledObject object;
	private String label;

	@BeforeEach
	void init() {
		this.object = new LabeledObject();
		this.label = UUID.randomUUID().toString();
	}

	@Test
	void testAddHasRemove() {
		Assertions.assertFalse(this.object.hasLabel(this.label));
		this.object.addLabel(this.label);
		Assertions.assertTrue(this.object.hasLabel(this.label));
		this.object.removeLabel(this.label);
		Assertions.assertFalse(this.object.hasLabel(this.label));
	}

	@Test
	void testGetLabels() {
		this.object.addLabel(this.label);
		Assertions.assertTrue(this.object.getLabels().contains(this.label));
	}

	@Test
	void testFindBy() {
		LabeledObject o1 = new LabeledObject();
		o1.addLabel(this.label);
		LabeledObject o2 = new LabeledObject();
		o2.addLabel(this.label + "-ext");

		List<LabeledObject> objects = LabeledObject.findByLabel(Arrays.asList(o1, o2), new SingleLabel(label));
		Assertions.assertTrue(objects.contains(o1));
		Assertions.assertFalse(objects.contains(o2));
	}
}
